
    angular.module('SAO.service.wsdl', [])
        .factory('wsdlService', ['$http', xsdService] );

    xsdService.$inject = ['$http'];
    function xsdService ($http) {
        var service = {getWsdl : getWsdl, getParametro : getParametro};
        return service;

        function getParametro(tipoParametro) {
            console.log("Buscando el parametro[" + tipoParametro + "]");

            var urlConsulta = 'http://170.1.1.65:7001/tesRest/rest/miAuto';
            return $http.get(urlConsulta);
        }

        function getWsdl() {
            var urlConsulta = 'http://localhost:8001/sentra/osb/wsdl/generar';
            return $http.get(urlConsulta);

            return $http(
                {url: urlConsulta,
                    method: 'GET',
                    responseType: 'application/json',
                })
            //$http(uploadUrl)
                .success(function(){
                    console.log("Success");
                    return this;
                })

                .error(function(){
                    console.log("Error...");
                    return this;
                });
        }
    }
