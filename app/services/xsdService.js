
    angular.module('SAO.service.xsd', [])
        .factory('xsdService', ['$http', xsdService] );

    xsdService.$inject = ['$http'];
    function xsdService ($http) {
        var service = {getXsd : getXsd, getParametro : getParametro};
        return service;

        function getParametro(tipoParametro) {
            console.log("Buscando el parametro[" + tipoParametro + "]");
            var urlConsulta = '/sao/parametro/'+tipoParametro;
            return $http.get(urlConsulta);
        }

        function getXsd(file) {
            var uploadUrl = '/sao/xsd';

            var fd = new FormData();
            fd.append('file', file);
            return $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
        }
    }
