'use strict';

// Declare app level module which depends on views, and components
angular.module('SAO', [
  'ngRoute',
  'SAO.vista.home',
  'SAO.vista.xsd',
  'SAO.vista.wsdl',
  'SAO.vista.manuales',
  'SAO.vista.templates',
  'SAO.vista.fuentes',
  'SAO.vista.instalacion',
  'SAO.service.xsd',
  'SAO.service.wsdl'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('');

  $routeProvider.otherwise({redirectTo: '/home'});
}]);
