'use strict';

angular.module('SAO.vista.instalacion', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/instalacion', {
            templateUrl: 'vistas/instalacion/instalacion.html',
            controller: 'InstalacionCtrl'
        });
    }])

    .controller('InstalacionCtrl', [function($scope) {

}]);