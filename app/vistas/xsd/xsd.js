'use strict';
(function () {
angular.module('SAO.vista.xsd', ['ngRoute', 'SAO.service.xsd'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/xsd', {
            templateUrl: 'vistas/xsd/xsd.html',
            controller: 'XsdCtrl',
            controllerAs: 'vm',
            directive: 'fileModel'
        });
    }])
    .controller('XsdCtrl', ['$scope', 'xsdService', controller])
    .directive('fileModel', ['$parse', directiva])

    /*
    function getParametroSucces(resultado) {
        console.log("exito getParametro");
    }

    function getParametroError(resultado) {
        console.log("error getParametro");
    }*/

    function controller($scope, xsdService, $http) {
        console.log("Inicio Generando XSD", xsdService);
        var vm = this;


        vm.irHome = function() {
            console.log("ir a home");
        }

        xsdService.getParametro("header").then(function(resultado) {
            console.log("success getParametro", resultado);
            vm.resultadoHeaders = resultado.data;
            //En caso de ser generado exitosamente el o los xsd y wsdl.
        }, function(error){
            if (error.status) {
                console.log("Hacer algo en caso de error...");
            }
            console.log("error getParametro", error);
        });

        xsdService.getParametro("error").then(function(resultado) {
            console.log("success getParametro", resultado);
            vm.resultadoErrores = resultado.data;
            //En caso de ser generado exitosamente el o los xsd y wsdl.
        }, function(error){
            if (error.status) {
                console.log("Hacer algo en caso de error...");
            }
            console.log("error getParametro", error);
        });


        //Elementos en la página del formulario de peticion y sus caracteristicas
        vm.formPeticion = {"mostrar": true, "titulo" : "Selecciona el o los arhivos excel (.xls) para generar el o los XSD y WSDL", "error": {}};
        vm.formPeticion.label = {"nombreWsdl": "Nombre:",
            "selectErrores":"Versión del control de error:",
            "selectHeaders":"Versión del header:",
            "excelEntrada":"Excel Entrada: ",
            "excelSalida": "Excel Salida: "};
        vm.formPeticion.input = {};
        vm.formPeticion.boton = {};
        vm.formPeticion.boton.enviar = {"label":"Generar", "disabled":false}
        vm.formPeticion.select = {};
        vm.formPeticion.select.error = {};
        vm.formPeticion.select.header = {};

        vm.formPeticion.select.error.item = "";
        vm.formPeticion.select.error.disabled = false;
        vm.formPeticion.select.header.item = "";
        vm.formPeticion.select.header.disabled = false;

        //Elementos del formulario de respuesta y sus caracteristicas.
        vm.formRespuesta = {"mostrar": false};
        vm.formRespuesta.resultado = {"xsd1": false, "xsd2": false, "wsdl": false};
        vm.formRespuesta.error = {};

        vm.limpiarErrorPagina = function() {
            console.log("limpiarErrorPagina");
            vm.formPeticion.error.visible = false;
        }

        vm.uploadFile = function($http) {
            console.log("Subiendo archivo...");
            var fileExcel1 = vm.fileExcel1;
            var fileExcel2 = vm.fileExcel2;
            if (fileExcel1 || fileExcel2) {
                vm.formPeticion.boton.enviar.disabled = true;
                vm.formPeticion.select.error.disabled = true;
                vm.formPeticion.select.header.disabled = true;
                vm.formPeticion.boton.enviar.label = "Generando...";

                var isFileExcel1 = false;
                var isFileExcel2 = false;
                if (fileExcel1) {
                    isFileExcel1 = true;
                    var extencion = fileExcel1.name.substring(fileExcel1.name.length-3);
                    console.log("EXT:", extencion);
                    if (extencion != "xls") {
                        vm.formPeticion.error.visible = true;
                        vm.formPeticion.error.label = "Archivo seleccionado debe ser Excel (.xls)";
                    }
                }

                if (fileExcel2) {
                    isFileExcel2 = true;
                    var extencion = fileExcel2.name.substring(fileExcel2.name.length-3);
                    console.log("EXT2:", extencion);
                    if (extencion != "xls") {
                        vm.formPeticion.error.visible = true;
                        vm.formPeticion.error.label = "Archivo seleccionado debe ser Excel (.xls)";
                    }
                }

                console.log('file is ', fileExcel1 );
                console.log('file2 is ', fileExcel2 );
                console.dir(fileExcel1);
                console.dir(fileExcel2);

                if (vm.formPeticion.input.wsdl == "") {
                    console.log("limpiando datos...", vm.formPeticion.input.wsdl);
                    vm.errorSeleccionado = "";
                    vm.headerSeleccionado = "";
                }

                if (fileExcel1 && fileExcel2) {
                    //llamada al un servicio que genera 2 xsd y wsdl.
                    xsdService.getXsd().then(function(resultado) {
                        console.log("1 resultado:", resultado);
                        //En caso de ser generado exitosamente el o los xsd y wsdl.

                    }, function(msg, code){
                        console.log("1 resultado error:" + msg + " - " + code);
                        //En caso de error se muestra el o los errores generados.
                        //var resultado = {"xsdEntrada": "http://www.sentra.cl/generarXSD/xsdEntrada.xsd"};
                        //var resultado = {"xsdEntrada": "http://www.sentra.cl/generarXSD/xsdEntrada.xsd", "xsdSalida":"http://www.sentra.cl/generarXSD/xsdSalida.xsd"};
                        var resultado = {"xsdEntrada": "http://www.sentra.cl/generarXSD/xsdEntrada.xsd", "xsdSalida":"http://www.sentra.cl/generarXSD/xsdSalida.xsd", "WSDL": "http://www.sentra.cl/generarXSD/wsdl.wsdl"};

                        var cantidad = 0;
                        if (resultado.xsdEntrada) {
                            cantidad = cantidad + 1;
                            vm.formRespuesta.resultado.xsd1 = true;
                        }
                        if (resultado.xsdSalida) {
                            cantidad = cantidad + 1;
                            vm.formRespuesta.resultado.xsd2 = true;
                        }
                        if (resultado.WSDL) {
                            cantidad = cantidad + 1;
                            vm.formRespuesta.resultado.wsdl = true;
                        }
                        vm.resultado = resultado;

                        vm.formPeticion.mostrar = false;
                        vm.formPeticion.boton.enviar.disabled = false;
                        vm.formPeticion.select.error.disabled = false;
                        vm.formPeticion.select.header.disabled = false;
                        vm.formPeticion.boton.enviar.label = "Generar";
                        vm.formRespuesta.mostrar = true;

                        if (cantidad > 0) {
                            if (cantidad == 1) {
                                vm.formRespuesta.titulo  = "Descargue el archivo generado.";
                            } else {
                                vm.formRespuesta.titulo  = "Descargue los archivos generados.";
                            }
                        } else {
                            vm.formRespuesta.titulo  = "Hubo un error para generar los archivos, contactese con el administrador el sistema llamando 800-122Sentra";
                        }
                    });
                } else {
                    var file = "";
                    if (fileExcel1) {
                        file = fileExcel1;
                    } else {
                        file = fileExcel2;
                    }

                    //llamada a un servicio que solo genera el xsd.
                    xsdService.getXsd(file).then(function(resultado) {
                        console.log("XSD Generado exitosamente.:", resultado);
                        if (resultado.xsdEntrada) {
                            vm.formRespuesta.resultado.xsd1 = true;
                            vm.formRespuesta.titulo  = "Descargue el archivo generado.";
                        }
                    }, function(error){
                        console.log("Error XSD 2:", error);
                        vm.formRespuesta.error.visible = true;
                        vm.formRespuesta.error.label = "Error Generando el XSD";
                    });

                }
            } else {
                vm.formPeticion.error.visible = true;
                vm.formPeticion.error.label = "Seleccione un archivo excel (.xls) para generar el XSD.";
            }

            vm.formPeticion.mostrar = false;
            vm.formPeticion.boton.enviar.disabled = false;
            vm.formPeticion.select.error.disabled = false;
            vm.formPeticion.select.header.disabled = false;
            vm.formPeticion.boton.enviar.label = "Generar";
            vm.formRespuesta.mostrar = true;
        }
    }

    function directiva($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }

})();
