'use strict';

angular.module('SAO.vista.fuentes', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/fuentes', {
            templateUrl: 'vistas/fuentes/fuentes.html',
            controller: 'FuentesCtrl'
        });
    }])

    .controller('FuentesCtrl', [function($scope) {

        function uploadFile() {
            //var file = $scope.myFile;
            alert("uploadFile");
            console.log('file is ' );
            //console.dir(file);

            var uploadUrl = "/fileUpload";
            console.info("uploadUrl:" + uploadUrl);
            //fileUpload.uploadFileToUrl(file, uploadUrl);
        };

}]);