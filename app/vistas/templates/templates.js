'use strict';

angular.module('SAO.vista.templates', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/templates', {
            templateUrl: 'vistas/templates/templates.html',
            controller: 'TemplatesCtrl'
        });
    }])

    .controller('TemplatesCtrl', [function($scope) {
        function uploadFile() {
            //var file = $scope.myFile;
            alert("uploadFile");
            console.log('file is ' );
            //console.dir(file);

            var uploadUrl = "/fileUpload";
            console.info("uploadUrl:" + uploadUrl);
            //fileUpload.uploadFileToUrl(file, uploadUrl);
        };

}]);