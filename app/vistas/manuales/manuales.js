'use strict';

angular.module('SAO.vista.manuales', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/manuales', {
            templateUrl: 'vistas/manuales/manuales.html',
            controller: 'ManualesCtrl'
        });
    }])

    .controller('ManualesCtrl', [function($scope) {

}]);