'use strict';
(function () {
    angular.module('SAO.vista.wsdl', ['ngRoute', 'SAO.service.wsdl'])

        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/wsdl', {
                templateUrl: 'vistas/wsdl/wsdl.html',
                controller: 'WsdlCtrl',
                controllerAs: 'vm',
                directive: 'fileModel'
            });
        }])
        .controller('WsdlCtrl', ['$scope', 'wsdlService', controller])
        .directive('fileModel', ['$parse', directiva])

    function controller($scope, wsdlService, $http) {
        console.log("Inicio Generando WSDL", wsdlService);
        var vm = this;

        vm.resultadoErrores = {"errores":
            [
                {"error":"http://osb.bancochile.cl/ent/bch/infra/mci/errorDetails/v/4"},
                {"error":"http://osb.bancochile.cl/ent/bch/infra/mci/errorDetails/v/5"}
            ]
        };

        vm.resultadoHeaders = {"headers":
            [
                {"header":"http://osb.bancochile.cl/ent/bch/infra/mci/errorDetails/v/4"},
                {"header":"http://osb.bancochile.cl/ent/bch/infra/mci/errorDetails/v/5"}
            ]
        };

        wsdlService.getParametro("error").then(function(resultado) {
            console.log("success getParametro", resultado);
            //En caso de ser generado exitosamente el o los xsd y wsdl.
        }, function(error){
            if (error.status) {
                console.log("ERROR -1");
            }
            console.log("error getParametro", error);
        });


        //Elementos en la página del formulario de peticion y sus caracteristicas
        vm.formPeticion = {"mostrar": true, "titulo" : "Selecciona los arhivos xsd (.xsd) para generar el WSDL"};
        vm.formPeticion.label = {"nombreWsdl": "Nombre:",
            "selectErrores":"Versión del control de error:",
            "selectHeaders":"Versión del header:",
            "excelEntrada":"XSD Entrada: ",
            "excelSalida": "XSD Salida: "};
        vm.formPeticion.input = {};
        vm.formPeticion.boton = {};
        vm.formPeticion.boton.enviar = {"label":"Generar", "disabled":false}
        vm.formPeticion.select = {};
        vm.formPeticion.select.error = {};
        vm.formPeticion.select.header = {};

        vm.formPeticion.select.error.item = "";
        vm.formPeticion.select.error.disabled = false;
        vm.formPeticion.select.header.item = "";
        vm.formPeticion.select.header.disabled = false;

        //Elementos del formulario de respuesta y sus caracteristicas.
        vm.formRespuesta = {"mostrar": false};
        vm.formRespuesta.resultado = {"xsd1": false, "xsd2": false, "wsdl": false};


        vm.uploadFile = function($http) {
            console.log("Subiendo archivo...");
            vm.formPeticion.boton.enviar.disabled = true;
            vm.formPeticion.select.error.disabled = true;
            vm.formPeticion.select.header.disabled = true;

            vm.formPeticion.boton.enviar.label = "Generando...";
            var file = vm.myFile1;
            var file2 = $scope.myFile2;
            console.log('file is ', file );
            console.log('file2 is ', file2 );
            console.dir(file);
            console.dir(file2);

            if (vm.formPeticion.input.wsdl == "") {
                console.log("limpiando datos...", vm.formPeticion.input.wsdl);
                vm.errorSeleccionado = "";
                vm.headerSeleccionado = "";
            }

            wsdlService.getWsdl().then(function(resultado) {
                console.log("1 resultado:", resultado);
                //En caso de ser generado exitosamente el o los xsd y wsdl.

            }, function(error){
                console.log("1 resultado error:", error);
                //En caso de error se muestra el o los errores generados.
                //var resultado = {"xsdEntrada": "http://www.sentra.cl/generarXSD/xsdEntrada.xsd"};
                //var resultado = {"xsdEntrada": "http://www.sentra.cl/generarXSD/xsdEntrada.xsd", "xsdSalida":"http://www.sentra.cl/generarXSD/xsdSalida.xsd"};
                var resultado = {"xsdEntrada": "http://www.sentra.cl/generarXSD/xsdEntrada.xsd", "xsdSalida":"http://www.sentra.cl/generarXSD/xsdSalida.xsd", "WSDL": "http://www.sentra.cl/generarXSD/wsdl.wsdl"};

                var cantidad = 0;
                if (resultado.xsdEntrada) {
                    cantidad = cantidad + 1;
                    vm.formRespuesta.resultado.xsd1 = true;
                }
                if (resultado.xsdSalida) {
                    cantidad = cantidad + 1;
                    vm.formRespuesta.resultado.xsd2 = true;
                }
                if (resultado.WSDL) {
                    cantidad = cantidad + 1;
                    vm.formRespuesta.resultado.wsdl = true;
                }
                vm.resultado = resultado;

                vm.formPeticion.mostrar = false;
                vm.formPeticion.boton.enviar.disabled = false;
                vm.formPeticion.select.error.disabled = false;
                vm.formPeticion.select.header.disabled = false;
                vm.formPeticion.boton.enviar.label = "Generar";
                vm.formRespuesta.mostrar = true;

                if (cantidad > 0) {
                    if (cantidad == 1) {
                        vm.formRespuesta.titulo  = "Descargue el archivo generado.";
                    } else {
                        vm.formRespuesta.titulo  = "Descargue los archivos generados.";
                    }
                } else {
                    vm.formRespuesta.titulo  = "Hubo un error para generar los archivos, contactese con el administrador el sistema llamando 800-122Sentra";
                }
                //vm.formRespuesta.resultado.xsd1 = true;
                //vm.formRespuesta.resultado.xsd2 = true;
                //vm.formRespuesta.resultado.wsdl = true;
            });
        }

    }

    function directiva($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }

})();
